LED PWM Readme
=-=-=-=-=-=-=-=
Control a 12-24V/120W pair of LED strings with a PWM signal.
The ideal string is a dual temperature white LED string:

	https://store.waveformlighting.com/collections/led-strips/products/filmgrade-hybrid-led-strip-lights

Orginally it was designed with a PWM controled/generated by a TinyPico (ESP32 micro-controller)

BUT - I decided that I want to prioritize an 'analog' control and interface *first* over dealing with the programming aspects.

HOWEVER - the LED section is still just driven by a PWM signal so the pins for the TinyPico *may* be included so that it can be easily updated/changed at a later date/revision.

---- V1 - Made ----
A Pi Pico (RP2040) uC was used to control generate the PWM signal.
Revisions to the board have been made based on the mistakes in the ones made.

Additional updates and thoughts will be noted as it is used.

---- V3 ----
PWM Generated by a trio of 555 timers
  * The first runs astable with a variable pulse-width (10-100us)
  * 

---- V1 & V2 ----
I honestly don't remember what the difference is.
What I do remember is:
  * PWM driven by TinyPico
  * TinyPico driven by 2 potentiometers (both have a switch on them)
  * The potentiometers are on a remote PCB that also has USB power on it - using DC-DC regulators (intended to be a bedside lamp)


Changlog
-- 19 July 2021 --
  * Created readme