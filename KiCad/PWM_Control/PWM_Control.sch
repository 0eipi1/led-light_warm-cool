EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L _MCU_RaspberryPi_and_Boards:Pico U1
U 1 1 61369500
P 1950 1750
F 0 "U1" H 1950 2965 50  0000 C CNN
F 1 "Pico" H 1950 2874 50  0000 C CNN
F 2 "lib_fp:RPi_Pico_SMD_TH" V 1950 1750 50  0001 C CNN
F 3 "" H 1950 1750 50  0001 C CNN
	1    1950 1750
	1    0    0    -1  
$EndComp
NoConn ~ 2650 800 
NoConn ~ 2650 1100
NoConn ~ 2650 1200
NoConn ~ 1900 3250
NoConn ~ 1250 2600
NoConn ~ 1250 2700
NoConn ~ 1850 2900
NoConn ~ 1950 2900
NoConn ~ 2050 2900
NoConn ~ 2650 2400
NoConn ~ 2650 2300
NoConn ~ 2650 2200
$Comp
L _6pin_header:2178710-6 J2
U 1 1 61370259
P 4100 900
F 0 "J2" H 4500 1165 50  0000 C CNN
F 1 "2178710-6" H 4500 1074 50  0000 C CNN
F 2 "lib_fp:6pin_header" H 4750 1000 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F2178710%7FA%7Fpdf%7FEnglish%7FENG_CD_2178710_A_pdf_c-2178710_drw.pdf%7F2178710-6" H 4750 900 50  0001 L CNN
F 4 "AMP - TE CONNECTIVITY - 2178710-6 - CONNECTOR, RCPT, 6POS, 2ROW, 1.27MM" H 4750 800 50  0001 L CNN "Description"
F 5 "4.2" H 4750 700 50  0001 L CNN "Height"
F 6 "571-2178710-6" H 4750 600 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/2178710-6?qs=tddCKBGf4%252BNTSMNMr%252BRjYw%3D%3D" H 4750 500 50  0001 L CNN "Mouser Price/Stock"
F 8 "TE Connectivity" H 4750 400 50  0001 L CNN "Manufacturer_Name"
F 9 "2178710-6" H 4750 300 50  0001 L CNN "Manufacturer_Part_Number"
	1    4100 900 
	1    0    0    -1  
$EndComp
Text GLabel 4100 900  0    50   Input ~ 0
Vin
Text GLabel 4050 4650 0    50   Input ~ 0
LED1_PWM
Text GLabel 4100 1000 0    50   Input ~ 0
LED2_PWM
$Comp
L power:GND #PWR0101
U 1 1 61370F4A
P 4900 900
F 0 "#PWR0101" H 4900 650 50  0001 C CNN
F 1 "GND" V 4905 772 50  0000 R CNN
F 2 "" H 4900 900 50  0001 C CNN
F 3 "" H 4900 900 50  0001 C CNN
	1    4900 900 
	0    -1   -1   0   
$EndComp
$Comp
L 3.3V-Reg:173950336 PS1
U 1 1 6137267E
P 4100 1450
F 0 "PS1" H 4828 1396 50  0000 L CNN
F 1 "173950336" H 4828 1305 50  0000 L CNN
F 2 "lib_fp:3.3V-Reg" H 4850 1550 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/173950336.pdf" H 4850 1450 50  0001 L CNN
F 4 "WURTH ELEKTRONIK - 173950336 - DC/DC Converter, 1 Output, 1.65 W, 3.3 V, 500 mA, MagI3C FDSM Series" H 4850 1350 50  0001 L CNN "Description"
F 5 "10.2" H 4850 1250 50  0001 L CNN "Height"
F 6 "Wurth Elektronik" H 4850 1150 50  0001 L CNN "Manufacturer_Name"
F 7 "173950336" H 4850 1050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "710-173950336" H 4850 950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Wurth-Elektronik/173950336?qs=d0WKAl%252BL4KZw6hIvUkPbpg%3D%3D" H 4850 850 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 4850 750 50  0001 L CNN "Arrow Part Number"
F 11 "" H 4850 650 50  0001 L CNN "Arrow Price/Stock"
	1    4100 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 61373036
P 4100 1550
F 0 "#PWR0102" H 4100 1300 50  0001 C CNN
F 1 "GND" V 4105 1422 50  0000 R CNN
F 2 "" H 4100 1550 50  0001 C CNN
F 3 "" H 4100 1550 50  0001 C CNN
	1    4100 1550
	0    1    1    0   
$EndComp
Text GLabel 4100 1450 0    50   Input ~ 0
Vin
Text GLabel 4100 1650 0    50   Input ~ 0
Vcc
Text GLabel 4900 1000 2    50   Input ~ 0
LED1_PWM_GND
Text GLabel 4900 1100 2    50   Input ~ 0
LED2_PWM_GND
Text GLabel 4100 1100 0    50   Input ~ 0
LED1_PWM
Text GLabel 4100 5500 0    50   Input ~ 0
LED2_PWM
Text GLabel 1150 1000 0    50   Input ~ 0
LED1_PWM_GND
Text GLabel 1150 1100 0    50   Input ~ 0
LED2_PWM_GND
Text GLabel 2650 900  2    50   Input ~ 0
Vcc
$Comp
L power:GND #PWR0103
U 1 1 61373F84
P 2650 1000
F 0 "#PWR0103" H 2650 750 50  0001 C CNN
F 1 "GND" V 2655 872 50  0000 R CNN
F 2 "" H 2650 1000 50  0001 C CNN
F 3 "" H 2650 1000 50  0001 C CNN
	1    2650 1000
	0    -1   -1   0   
$EndComp
$Comp
L Reference_Voltage:LM4040LP-3 U2
U 1 1 61377966
P 4450 2250
F 0 "U2" H 4450 2466 50  0000 C CNN
F 1 "LM4040LP-3" H 4450 2375 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4450 2050 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm4040-n.pdf" H 4450 2250 50  0001 C CIN
	1    4450 2250
	0    -1   -1   0   
$EndComp
Text GLabel 2650 1300 2    50   Input ~ 0
ADC_VREF
Text GLabel 2650 1500 2    50   Input ~ 0
AGND
Text GLabel 4050 1950 0    50   Input ~ 0
ADC_VREF
Text GLabel 4050 2550 0    50   Input ~ 0
AGND
Wire Wire Line
	4050 2550 4450 2550
Wire Wire Line
	4450 2550 4450 2400
Wire Wire Line
	4050 1950 4450 1950
Wire Wire Line
	4450 1950 4450 2100
Text GLabel 2650 1400 2    50   Input ~ 0
ADC_GND
Text GLabel 2650 1700 2    50   Input ~ 0
ADC_Brightness
Text GLabel 2650 1600 2    50   Input ~ 0
ADC_Color
$Comp
L Device:R_POT RV1
U 1 1 6137AA68
P 4950 2250
F 0 "RV1" H 4880 2296 50  0000 R CNN
F 1 "10k" H 4880 2205 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_PTA6043_Single_Slide" H 4950 2250 50  0001 C CNN
F 3 "~" H 4950 2250 50  0001 C CNN
	1    4950 2250
	1    0    0    -1  
$EndComp
Text GLabel 5100 2250 2    50   Input ~ 0
ADC_Brightness
$Comp
L Device:R_POT RV2
U 1 1 6137C0F4
P 6250 2250
F 0 "RV2" H 6180 2296 50  0000 R CNN
F 1 "10k" H 6180 2205 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_PTA6043_Single_Slide" H 6250 2250 50  0001 C CNN
F 3 "~" H 6250 2250 50  0001 C CNN
	1    6250 2250
	1    0    0    -1  
$EndComp
Text GLabel 6400 2250 2    50   Input ~ 0
ADC_Color
Wire Wire Line
	4450 1950 4950 1950
Wire Wire Line
	4950 1950 4950 2100
Connection ~ 4450 1950
Wire Wire Line
	4950 1950 6250 1950
Wire Wire Line
	6250 1950 6250 2100
Connection ~ 4950 1950
Wire Wire Line
	4450 2550 4950 2550
Wire Wire Line
	4950 2550 4950 2400
Connection ~ 4450 2550
Wire Wire Line
	4950 2550 6250 2550
Wire Wire Line
	6250 2550 6250 2400
Connection ~ 4950 2550
Text GLabel 4050 2650 0    50   Input ~ 0
ADC_GND
Wire Wire Line
	4050 2650 4450 2650
Wire Wire Line
	4450 2650 4450 2550
NoConn ~ 2650 1900
Text GLabel 2650 1800 2    50   Input ~ 0
RST
Text GLabel 1250 2400 0    50   Input ~ 0
RTC_SCL
Text GLabel 1250 2300 0    50   Input ~ 0
RTC_SDA
Text GLabel 1250 1900 0    50   Input ~ 0
Mode_Switch
$Comp
L B3F-4000:B3F-4000 S1
U 1 1 61383A42
P 4200 3150
F 0 "S1" H 4850 3415 50  0000 C CNN
F 1 "B3F-1000S" H 4850 3324 50  0000 C CNN
F 2 "lib_fp:B3F-1000" H 5350 3250 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/307/en-b3f-13826.pdf" H 5350 3150 50  0001 L CNN
F 4 "Tactile Switches 6x6 Flat 4.3mm Btn Force 100g w/o Grd" H 5350 3050 50  0001 L CNN "Description"
F 5 "7" H 5350 2950 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 5350 2850 50  0001 L CNN "Manufacturer_Name"
F 7 "B3F-1000S" H 5350 2750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "653-B3F-1000-S" H 5350 2650 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Omron-Electronics/B3F-1000S?qs=C6KiJco3hx0Z3qfxWTJOrg%3D%3D" H 5350 2550 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5350 2450 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5350 2350 50  0001 L CNN "Arrow Price/Stock"
	1    4200 3150
	1    0    0    -1  
$EndComp
Text GLabel 4150 3150 0    50   Input ~ 0
RST
Text GLabel 4150 3250 0    50   Input ~ 0
RST_GND
Text GLabel 4100 3900 0    50   Input ~ 0
Mode_Switch
Text GLabel 4100 4000 0    50   Input ~ 0
Mode_GND
Text GLabel 3850 3750 0    50   Input ~ 0
Vcc
$Comp
L Device:R R1
U 1 1 6138611D
P 4050 3750
F 0 "R1" V 3843 3750 50  0000 C CNN
F 1 "10k" V 3934 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3980 3750 50  0001 C CNN
F 3 "~" H 4050 3750 50  0001 C CNN
	1    4050 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 3750 3900 3750
Wire Wire Line
	4200 3750 4200 3900
Connection ~ 4200 3900
Wire Wire Line
	4100 3900 4200 3900
Wire Wire Line
	4100 4000 4200 4000
Text GLabel 1250 800  0    50   Input ~ 0
Pico_PWM_1
Text GLabel 1250 900  0    50   Input ~ 0
Pico_PWM_2
Text GLabel 4100 5200 0    50   Input ~ 0
Pico_PWM_2
Text GLabel 4050 4400 0    50   Input ~ 0
Pico_PWM_1
$Comp
L Jumper:Jumper_3_Open JP1
U 1 1 6138DB2F
P 4700 4650
F 0 "JP1" H 4700 4781 50  0000 C CNN
F 1 "Jumper_3_Open" H 4700 4872 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4700 4650 50  0001 C CNN
F 3 "~" H 4700 4650 50  0001 C CNN
	1    4700 4650
	-1   0    0    1   
$EndComp
$Comp
L Jumper:Jumper_3_Open JP2
U 1 1 6138F211
P 4700 5500
F 0 "JP2" H 4700 5631 50  0000 C CNN
F 1 "Jumper_3_Open" H 4700 5722 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4700 5500 50  0001 C CNN
F 3 "~" H 4700 5500 50  0001 C CNN
	1    4700 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4050 4400 4700 4400
Wire Wire Line
	4700 4400 4700 4500
Wire Wire Line
	4450 4650 4050 4650
Wire Wire Line
	4100 5200 4700 5200
Wire Wire Line
	4700 5200 4700 5350
Wire Wire Line
	4450 5500 4100 5500
$Comp
L Device:LED D1
U 1 1 61391D8D
P 5300 4650
F 0 "D1" V 5339 4532 50  0000 R CNN
F 1 "LED" V 5248 4532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 5300 4650 50  0001 C CNN
F 3 "~" H 5300 4650 50  0001 C CNN
	1    5300 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 61392633
P 5300 5500
F 0 "D2" V 5339 5382 50  0000 R CNN
F 1 "LED" V 5248 5382 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 5300 5500 50  0001 C CNN
F 3 "~" H 5300 5500 50  0001 C CNN
	1    5300 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4950 4650 5150 4650
$Comp
L power:GND #PWR0104
U 1 1 613943A0
P 5450 4650
F 0 "#PWR0104" H 5450 4400 50  0001 C CNN
F 1 "GND" H 5455 4477 50  0000 C CNN
F 2 "" H 5450 4650 50  0001 C CNN
F 3 "" H 5450 4650 50  0001 C CNN
	1    5450 4650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 613953AE
P 5450 5500
F 0 "#PWR0105" H 5450 5250 50  0001 C CNN
F 1 "GND" H 5455 5327 50  0000 C CNN
F 2 "" H 5450 5500 50  0001 C CNN
F 3 "" H 5450 5500 50  0001 C CNN
	1    5450 5500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 5500 5150 5500
$Comp
L 3013:3013 J1
U 1 1 613A1E07
P 3950 6000
F 0 "J1" H 4058 6481 50  0000 C CNN
F 1 "RTC" H 4058 6390 50  0000 C CNN
F 2 "lib_fp:3013" H 3950 6000 50  0001 C CNN
F 3 "~" H 3950 6000 50  0001 C CNN
	1    3950 6000
	-1   0    0    -1  
$EndComp
Text GLabel 4200 6000 2    50   Input ~ 0
Vcc
Wire Wire Line
	4200 6000 3950 6000
Wire Wire Line
	4250 6100 3950 6100
Text GLabel 4250 6200 2    50   Input ~ 0
RTC_SCL
Text GLabel 4250 6300 2    50   Input ~ 0
RTC_SDA
Wire Wire Line
	4250 6200 3950 6200
Wire Wire Line
	4250 6300 3950 6300
NoConn ~ 3950 6400
NoConn ~ 3950 6500
NoConn ~ 3950 6600
NoConn ~ 3950 6700
NoConn ~ 2850 6000
NoConn ~ 2850 6100
$Comp
L Mechanical:MountingHole H1
U 1 1 6139E937
P 6750 4650
F 0 "H1" H 6850 4696 50  0000 L CNN
F 1 "MountingHole" H 6850 4605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 4650 50  0001 C CNN
F 3 "~" H 6750 4650 50  0001 C CNN
	1    6750 4650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 6139ED36
P 6750 4850
F 0 "H2" H 6850 4896 50  0000 L CNN
F 1 "MountingHole" H 6850 4805 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 4850 50  0001 C CNN
F 3 "~" H 6750 4850 50  0001 C CNN
	1    6750 4850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 6139F0ED
P 6750 5050
F 0 "H3" H 6850 5096 50  0000 L CNN
F 1 "MountingHole" H 6850 5005 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 5050 50  0001 C CNN
F 3 "~" H 6750 5050 50  0001 C CNN
	1    6750 5050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 6139F4E3
P 6750 5250
F 0 "H4" H 6850 5296 50  0000 L CNN
F 1 "MountingHole" H 6850 5205 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 5250 50  0001 C CNN
F 3 "~" H 6750 5250 50  0001 C CNN
	1    6750 5250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 613B82E2
P 6750 5450
F 0 "H5" H 6850 5496 50  0000 L CNN
F 1 "MountingHole" H 6850 5405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 5450 50  0001 C CNN
F 3 "~" H 6750 5450 50  0001 C CNN
	1    6750 5450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 613B8652
P 6750 5650
F 0 "H6" H 6850 5696 50  0000 L CNN
F 1 "MountingHole" H 6850 5605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 5650 50  0001 C CNN
F 3 "~" H 6750 5650 50  0001 C CNN
	1    6750 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3550 4200 3750
Connection ~ 4200 3750
Wire Wire Line
	5500 4000 5500 4150
Connection ~ 4200 4000
Wire Wire Line
	4150 3150 4200 3150
Connection ~ 4200 3150
Wire Wire Line
	4150 3250 4200 3250
Connection ~ 4200 3250
Wire Wire Line
	4200 4250 5800 4250
Wire Wire Line
	5800 4250 5800 4000
Wire Wire Line
	4200 4000 4200 4250
Wire Wire Line
	5800 3550 5800 3900
Wire Wire Line
	4200 3550 5800 3550
Wire Wire Line
	4200 3500 5800 3500
Wire Wire Line
	5800 3500 5800 3250
Wire Wire Line
	4200 3250 4200 3500
Wire Wire Line
	4200 2850 5800 2850
Wire Wire Line
	5800 2850 5800 3150
Wire Wire Line
	4200 2850 4200 3150
$Comp
L Mechanical:MountingHole H7
U 1 1 61416D5B
P 6750 5850
F 0 "H7" H 6850 5896 50  0000 L CNN
F 1 "MountingHole" H 6850 5805 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 5850 50  0001 C CNN
F 3 "~" H 6750 5850 50  0001 C CNN
	1    6750 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H8
U 1 1 61417131
P 6750 6050
F 0 "H8" H 6850 6096 50  0000 L CNN
F 1 "MountingHole" H 6850 6005 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 6750 6050 50  0001 C CNN
F 3 "~" H 6750 6050 50  0001 C CNN
	1    6750 6050
	1    0    0    -1  
$EndComp
Text GLabel 4250 6100 2    50   Input ~ 0
RTC_GND
Wire Wire Line
	1150 1100 1200 1100
Wire Wire Line
	1200 1100 1200 1000
Wire Wire Line
	1200 1000 1150 1000
Wire Wire Line
	1200 1000 1250 1000
Connection ~ 1200 1000
$Comp
L power:GND #PWR0106
U 1 1 6141E2D8
P 1150 1500
F 0 "#PWR0106" H 1150 1250 50  0001 C CNN
F 1 "GND" V 1155 1372 50  0000 R CNN
F 2 "" H 1150 1500 50  0001 C CNN
F 3 "" H 1150 1500 50  0001 C CNN
	1    1150 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 1500 1250 1500
Text GLabel 1250 2500 0    50   Input ~ 0
RTC_GND
Text GLabel 1250 2000 0    50   Input ~ 0
Mode_GND
Text GLabel 2650 2000 2    50   Input ~ 0
RST_GND
$Comp
L B3F-4000:B3F-4000 S2
U 1 1 61384FEC
P 4200 3900
F 0 "S2" H 4850 4165 50  0000 C CNN
F 1 "B3F-1000S" H 4850 4074 50  0000 C CNN
F 2 "lib_fp:B3F-4000" H 5350 4000 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/307/en-b3f-13826.pdf" H 5350 3900 50  0001 L CNN
F 4 "Tactile Switches 6x6 Flat 4.3mm Btn Force 100g w/o Grd" H 5350 3800 50  0001 L CNN "Description"
F 5 "7" H 5350 3700 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 5350 3600 50  0001 L CNN "Manufacturer_Name"
F 7 "B3F-1000S" H 5350 3500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "653-B3F-1000-S" H 5350 3400 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Omron-Electronics/B3F-1000S?qs=C6KiJco3hx0Z3qfxWTJOrg%3D%3D" H 5350 3300 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5350 3200 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5350 3100 50  0001 L CNN "Arrow Price/Stock"
	1    4200 3900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
