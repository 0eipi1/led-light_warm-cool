# Write your code here :-)
import board
import analogio
import digitalio
import pwmio
import time
import math

TEST = False


# set the ADC pins
adc_GND = analogio.AnalogIn(board.A2)
adc_BRT = analogio.AnalogIn(board.A0)
adc_CLR = analogio.AnalogIn(board.A1)
if TEST:
    print(f'GND: {adc_GND.value/2**16:.2f}')
    print(f'BRT: {adc_BRT.value/2**16:.2f}')
    print(f'CLR: {adc_CLR.value/2**16:.2f}')

# setup the RTC
pass

# set the PWM pins
pwm_warm = pwmio.PWMOut(board.GP1, frequency=1000)
pwm_cool = pwmio.PWMOut(board.GP0, frequency=1000)

# setup the state_switch
state_switch = digitalio.DigitalInOut(board.GP9)
print(dir(digitalio))

state = 'off'
t0 = time.monotonic_ns()
# read the sliders
gnd = adc_GND.value
brt = adc_BRT.value / 2**16
clr = adc_CLR.value / 2**16

# apply square law of brightness
brt = brt ** 2

# calculate warm
if clr <= 0.6:
    warm = 1
else:
    warm = 1/0.4 - clr / 0.40
setting = int(brt* warm * 2**16 - 1)
pwm_warm.duty_cycle = setting
if TEST:
    print(f'Warm: {setting / 2**16}')


# calculate cool
if clr >= 0.4:
    cool =1
else:
    cool = clr / 0.4
setting = int(brt* cool * 2**16 - 1)
pwm_cool.duty_cycle = setting
if TEST:
    print(f'Cool: {setting / 2**16}')

if TEST:
    time_ms = (time.monotonic_ns() - t0) / 10**6
    print(f'Time it took (ms): {time_ms}')

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

while True:
    #time.sleep(0.1)
    #check the switch state
    if state_switch.value == False:
        if state == 'off':
            state = 'manual'
            led.value = True

        elif state == 'manual':
            state = 'off'
            led.value = False
        #elif state == 'auto':

        # add a pause when the sate is changed
        time.sleep(1)

    # print the state of the mode
    if TEST:
        print(f'The current mode is: {state}')

    if state == 'off':
        pass

    elif state == 'manual':
        # read the sliders
        gnd = adc_GND.value
        brt = adc_BRT.value / 2**16
        clr = adc_CLR.value / 2**16

        # apply square law of brightness
        brt = brt ** 2

        # calculate warm
        if clr <= 0.6:
            warm = 1
        else:
            warm = 1/0.4 - clr / 0.40
        setting = int(brt* warm * 2**16 - 1)
        pwm_warm.duty_cycle = setting
        if TEST:
            print(f'Warm: {setting / 2**16}')


        # calculate cool
        if clr >= 0.4:
            cool =1
        else:
            cool = clr / 0.4
        setting = int(brt* cool * 2**16 - 1)
        pwm_cool.duty_cycle = setting
        if TEST:
            print(f'Cool: {setting / 2**16}')
    # Write your code here :-)
